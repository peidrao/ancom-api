from django.contrib import admin
from product.models import Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ("code", "description", "value", "commission_percentage")
    search_fields = ("code", "description")
    list_filter = ("commission_percentage",)
    fieldsets = (
        (
            "Product Information",
            {"fields": ("code", "description", "value", "commission_percentage")},
        ),
    )


admin.site.register(Product, ProductAdmin)
