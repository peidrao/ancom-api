from decimal import Decimal
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Product(models.Model):
    code = models.CharField(max_length=100)
    description = models.CharField(max_length=250)
    value: Decimal = models.DecimalField(
        verbose_name="valor unitário",
        max_digits=17 + 5,
        decimal_places=5,
    )
    commission_percentage: Decimal = models.DecimalField(
        verbose_name="Percentual de Comissão",
        max_digits=4,
        decimal_places=2,
        validators=[MinValueValidator(0), MaxValueValidator(10)],
    )

    def __str__(self) -> str:
        return self.description
