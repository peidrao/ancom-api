from django.test import TestCase
from rest_framework.exceptions import ValidationError
from product.serializers import ProductSerializer


class ProductSerializerTests(TestCase):
    def test_valid_data(self):
        valid_data = {
            "code": "PROD1",
            "description": "Product 1",
            "value": "10.00000",
            "commission_percentage": "5.00",
        }

        serializer = ProductSerializer(data=valid_data)
        self.assertTrue(serializer.is_valid())

    def test_missing_required_fields(self):
        invalid_data = {
            "description": "Product 1",
            "value": "10.00000",
            "commission_percentage": "5.00",
        }

        serializer = ProductSerializer(data=invalid_data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_invalid_value(self):
        invalid_data = {
            "code": "PROD1",
            "description": "Product 1",
            "value": "invalid_value",
            "commission_percentage": "5.00",
        }

        serializer = ProductSerializer(data=invalid_data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_invalid_commission_percentage(self):
        # Invalid value for 'commission_percentage'
        invalid_data = {
            "code": "PROD1",
            "description": "Product 1",
            "value": "10.00000",
            "commission_percentage": "invalid_value",
        }

        serializer = ProductSerializer(data=invalid_data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)
