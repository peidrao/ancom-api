from django.test import TestCase
from decimal import Decimal
from django.core.exceptions import ValidationError
from product.models import Product


class ProductModelTestCase(TestCase):
    def test_create_product(self):
        product = Product.objects.create(
            code="P001",
            description="Test Product",
            value=Decimal("100.00"),
            commission_percentage=Decimal("5.0"),
        )

        self.assertEqual(product.__str__(), product.description)
        self.assertEqual(product.code, "P001")
        self.assertEqual(product.description, "Test Product")
        self.assertEqual(product.value, Decimal("100.00"))
        self.assertEqual(product.commission_percentage, Decimal("5.0"))

    def test_commission_percentage_validation(self):
        with self.assertRaises(ValidationError):
            product = Product(
                code="P002",
                description="Test Product 2",
                value=Decimal("200.00"),
                commission_percentage=Decimal("-1.0"),
            )
            product.full_clean()

        with self.assertRaises(ValidationError):
            product = Product(
                code="P003",
                description="Test Product 3",
                value=Decimal("300.00"),
                commission_percentage=Decimal("10.1"),
            )
            product.full_clean()

        product = Product(
            code="P004",
            description="Test Product 4",
            value=Decimal("400.00"),
            commission_percentage=Decimal("5.0"),
        )
        product.full_clean()
