from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from product.models import Product


class ProductViewSetTests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_product(self):
        data = {
            "code": "PROD1",
            "description": "Product 1",
            "value": "10.00000",
            "commission_percentage": "5.00",
        }
        response = self.client.post(reverse("product-list"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_product_list(self):
        response = self.client.get(reverse("product-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_product(self):
        product = Product.objects.create(
            code="PROD1",
            description="Product 1",
            value="10.00000",
            commission_percentage="5.00",
        )
        response = self.client.get(reverse("product-detail", args=[product.pk]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_product(self):
        product = Product.objects.create(
            code="PROD1",
            description="Product 1",
            value="10.00000",
            commission_percentage="5.00",
        )
        updated_data = {
            "code": "PROD1",
            "description": "Updated Product",
            "value": "12.00000",
            "commission_percentage": "7.50",
        }
        response = self.client.put(
            reverse("product-detail", args=[product.pk]), updated_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_product(self):
        product = Product.objects.create(
            code="PROD1",
            description="Product 1",
            value="10.00000",
            commission_percentage="5.00",
        )
        response = self.client.delete(reverse("product-detail", args=[product.pk]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
