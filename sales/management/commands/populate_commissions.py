from django.core.management.base import BaseCommand
from commissions.models import CommissionConfig, DayChoices


class Command(BaseCommand):
    help = "Setup commission configurations for each day of the week."

    def handle(self, *args, **kwargs):
        CommissionConfig.objects.all().delete()

        commission_configs = [
            {"day_of_week": day[0], "min_commission": 0, "max_commission": 0}
            for day in DayChoices.choices
        ]

        for config in commission_configs:
            CommissionConfig.objects.create(**config)

        self.stdout.write(self.style.SUCCESS("Commissions set up successfully."))
