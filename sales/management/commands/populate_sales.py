import random
import warnings

from datetime import datetime
from django.core.management.base import BaseCommand
from faker import Faker
from persons.models import Customer, SalesPerson
from product.models import Product
from sales.models import Sale, SaleItem

fake = Faker()
warnings.filterwarnings("ignore", message="DateTimeField .* received a naive datetime")


class Command(BaseCommand):
    help = "Populate data with random values"

    def handle(self, *args, **kwargs):
        num_customers = 10
        num_salespersons = 5
        num_products = 10
        num_sales = 10
        num_sale_items_per_sale = 4

        for _ in range(num_customers):
            name = fake.name()
            Customer.objects.create(
                name=name,
                email=f"{name}@amcom.com",
                phone=fake.phone_number(),
            )

        for _ in range(num_salespersons):
            name = fake.name()
            SalesPerson.objects.create(
                name=fake.name(),
                email=f"{name}@amcom.com",
                phone=fake.phone_number(),
            )

        for _ in range(num_products):
            Product.objects.create(
                code=fake.bothify(text="COD-??-##"),
                description=fake.sentence(nb_words=5, variable_nb_words=False),
                value=round(random.uniform(10, 1000), 2),
                commission_percentage=round(random.uniform(0, 10), 2),
            )

        for _ in range(num_sales):
            customer = random.choice(Customer.objects.all())
            salesperson = random.choice(SalesPerson.objects.all())
            sale = Sale.objects.create(
                invoice_number=fake.ean(length=13),
                customer=customer,
                sales_person=salesperson,
                created_at=fake.date_between_dates(
                    date_start=datetime(2013, 1, 1), date_end=datetime(2023, 12, 31)
                ),
            )

            for _ in range(num_sale_items_per_sale):
                product = random.choice(Product.objects.all())
                quantity = random.randint(1, 10)
                SaleItem.objects.create(
                    sale=sale,
                    product=product,
                    quantity=quantity,
                    commission=round(random.uniform(0, 10), 2),
                )

        self.stdout.write(self.style.SUCCESS("Data populated successfully."))
