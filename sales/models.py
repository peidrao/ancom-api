from django.db import models, transaction
from product.models import Product
from decimal import Decimal
from django.core.validators import MaxValueValidator, MinValueValidator
from persons.models import Customer, SalesPerson


class Sale(models.Model):
    invoice_number = models.CharField(
        max_length=50, verbose_name="Número da nota fiscal"
    )
    customer = models.ForeignKey(
        Customer, on_delete=models.PROTECT, verbose_name="Cliente"
    )
    sales_person = models.ForeignKey(
        SalesPerson, on_delete=models.PROTECT, verbose_name="Vendedor"
    )
    products = models.ManyToManyField(
        Product, through="SaleItem", verbose_name="Produtos"
    )
    created_at = models.DateTimeField(verbose_name="Data/Hora")

    def __str__(self):
        return f"Sale {self.invoice_number}"

    def delete(self, *args, **kwargs):
        with transaction.atomic():
            self.saleitem_set.all().delete()
            super().delete(*args, **kwargs)


class SaleItem(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    commission: Decimal = models.DecimalField(
        verbose_name="Percentual de Comissão",
        max_digits=4,
        decimal_places=2,
        validators=[MinValueValidator(0), MaxValueValidator(10)],
    )
    quantity = models.PositiveIntegerField(verbose_name="Quantidades Vendidas")

    def __str__(self):
        return f"{self.quantity} x {self.product.code}"

    @property
    def total_value(self):
        return self.product.value * self.quantity

    @property
    def commission_value(self):
        return (self.total_value * self.commission) / 100
