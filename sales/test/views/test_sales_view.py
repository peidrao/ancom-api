import json
from django.urls import reverse
from datetime import datetime
from rest_framework import status
from rest_framework.test import APITestCase

from sales.models import Sale, SaleItem
from product.models import Product
from persons.models import Customer, SalesPerson
from commissions.models import CommissionConfig


class SaleViewSetTestCase(APITestCase):
    def setUp(self):
        self.product1 = Product.objects.create(
            code="PROD1", description="Product 1", value=10.0, commission_percentage=5
        )
        self.product2 = Product.objects.create(
            code="PROD2", description="Product 2", value=15.0, commission_percentage=7
        )

        self.customer = Customer.objects.create(name="Customer Test")
        self.salesperson = SalesPerson.objects.create(name="SalesPerson Test")

    def test_create_sale(self):
        url = reverse("sale-list")
        created_at = datetime.now()
        data = {
            "customer": self.customer.id,
            "sales_person": self.salesperson.id,
            "created_at": str(created_at),
            "products": [
                {"product": {"id": self.product1.id}, "quantity": 3},
                {"product": {"id": self.product2.id}, "quantity": 2},
            ],
        }

        response = self.client.post(
            url, json.dumps(data), content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Sale.objects.count(), 1)

        self.assertEqual(SaleItem.objects.count(), 2)

    def test_commission_calculation(self):
        today = datetime.now().strftime("%A").lower()
        created_at = datetime.now()
        CommissionConfig.objects.create(
            day_of_week=today, min_commission=2, max_commission=8, is_active=True
        )

        url = reverse("sale-list")
        data = {
            "customer": self.customer.id,
            "sales_person": self.salesperson.id,
            "created_at": str(created_at),
            "products": [
                {"product": {"id": self.product1.id}, "quantity": 3},
                {"product": {"id": self.product2.id}, "quantity": 2},
            ],
        }

        response = self.client.post(
            url, json.dumps(data), content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Sale.objects.count(), 1)

        self.assertEqual(SaleItem.objects.count(), 2)

    def test_create_sale_no_products(self):
        url = reverse("sale-list")
        data = {
            "customer": 1,
            "sales_person": 1,
            "created_at": "2023-09-12T14:30:00Z",
            "products": [],
        }

        response = self.client.post(
            url, json.dumps(data), content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["message"], "Não existem produtos nessa venda.")
        self.assertEqual(Sale.objects.count(), 0)
