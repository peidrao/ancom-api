from datetime import datetime
from django.test import TestCase
from sales.models import Sale, SaleItem
from persons.models import Customer, SalesPerson
from product.models import Product
from django.db import transaction


class SaleModelTests(TestCase):
    def setUp(self):
        self.customer = Customer.objects.create(
            name="Alice Smith", email="alice@example.com", phone="1234567890"
        )
        self.sales_person = SalesPerson.objects.create(
            name="Bob Johnson", email="bob@example.com", phone="9876543210"
        )
        self.sale = Sale.objects.create(
            invoice_number="INV123",
            customer=self.customer,
            sales_person=self.sales_person,
            created_at=datetime.now(),
        )
        self.product = Product.objects.create(
            code="PROD1", description="Product 1", value=10.0, commission_percentage=4.0
        )

    def test_sale_creation(self):
        self.assertEqual(self.sale.invoice_number, "INV123")
        self.assertEqual(self.sale.customer, self.customer)
        self.assertEqual(self.sale.sales_person, self.sales_person)

    def test_sale_str_method(self):
        self.assertEqual(str(self.sale), f"Sale {self.sale.invoice_number}")

    def test_sale_products_through(self):
        product = Product.objects.create(
            code="PROD1", description="Product 1", value=10.0, commission_percentage=5.0
        )
        SaleItem.objects.create(
            sale=self.sale, product=product, quantity=2, commission=1.0
        )
        self.assertEqual(self.sale.products.count(), 1)

    def test_sale_created_at_auto_now(self):
        self.assertIsNotNone(self.sale.created_at)

    def test_sale_products_total_quantity(self):
        product1 = Product.objects.create(
            code="PROD1", description="Product 1", value=10.0, commission_percentage=2.0
        )
        product2 = Product.objects.create(
            code="PROD2", description="Product 2", value=20.0, commission_percentage=1.0
        )
        SaleItem.objects.create(
            sale=self.sale, product=product1, quantity=2, commission=1.0
        )
        SaleItem.objects.create(
            sale=self.sale, product=product2, quantity=3, commission=1.0
        )
        total_quantity = sum(item.quantity for item in self.sale.saleitem_set.all())
        self.assertEqual(total_quantity, 5)

    def test_sale_products_total_price(self):
        product2 = Product.objects.create(
            code="PROD2", description="Product 2", value=20.0, commission_percentage=5.0
        )
        SaleItem.objects.create(
            sale=self.sale, product=self.product, quantity=2, commission=1.0
        )
        SaleItem.objects.create(
            sale=self.sale, product=product2, quantity=3, commission=1.0
        )
        total_price = sum(
            item.quantity * item.product.value for item in self.sale.saleitem_set.all()
        )
        self.assertEqual(total_price, 2 * 10.0 + 3 * 20.0)

    def test_delete_sale_succeeds_if_there_are_no_related_sale_items(self):
        self.sale.delete()
        self.assertFalse(Sale.objects.exists())

    def test_delete_sale(self):
        SaleItem.objects.create(
            sale=self.sale, product=self.product, quantity=2, commission=4
        )

        self.assertEqual(Sale.objects.count(), 1)
        self.assertEqual(SaleItem.objects.count(), 1)

        with transaction.atomic():
            self.sale.delete()

        self.assertEqual(Sale.objects.count(), 0)
        self.assertEqual(SaleItem.objects.count(), 0)


class SaleItemModelTests(TestCase):
    def setUp(self):
        self.customer = Customer.objects.create(name="Customer Test")
        self.sales_person = SalesPerson.objects.create(name="Sales Person Test")
        self.product = Product.objects.create(
            code="PROD1", description="Product 1", value=10.0, commission_percentage=5.0
        )
        self.sale = Sale.objects.create(
            invoice_number="INV123",
            customer=self.customer,
            sales_person=self.sales_person,
            created_at=datetime.now(),
        )

    def test_total_value(self):
        sale_item = SaleItem.objects.create(
            sale=self.sale, product=self.product, commission=5.0, quantity=2
        )
        total_value = sale_item.total_value

        self.assertEqual(total_value, 20.0)

    def test_sale_item_str(self):
        sale_item = SaleItem.objects.create(
            sale=self.sale, product=self.product, commission=5.0, quantity=2
        )

        self.assertEqual(
            sale_item.__str__(), f"{sale_item.quantity} x {sale_item.product.code}"
        )

    def test_commission_value(self):
        sale_item = SaleItem.objects.create(
            sale=self.sale, product=self.product, commission=5.0, quantity=2
        )
        commission_value = sale_item.commission_value

        self.assertEqual(commission_value, 1.0)
