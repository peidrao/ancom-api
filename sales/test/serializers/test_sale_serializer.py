from datetime import datetime
from django.test import TestCase
from rest_framework.exceptions import ValidationError
from sales.serializers import SalesCreateSerializer, SaleSerializer
from sales.models import Sale, SaleItem
from product.models import Product
from persons.models import Customer, SalesPerson


class SalesCreateSerializerTest(TestCase):
    def test_valid_serializer(self):
        data = {"customer": 1, "sales_person": 2, "created_at": "2023-09-15T12:00:00"}
        serializer = SalesCreateSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_invalid_serializer_missing_field(self):
        data = {"customer": 1, "sales_person": 2}
        serializer = SalesCreateSerializer(data=data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_invalid_serializer_wrong_data_type(self):
        data = {
            "customer": "not_an_integer",
            "sales_person": 2,
            "created_at": "2023-09-15T12:00:00",
        }
        serializer = SalesCreateSerializer(data=data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_invalid_serializer_wrong_datetime_format(self):
        data = {"customer": 1, "sales_person": 2, "created_at": "15-09-2023 12:00:00"}
        serializer = SalesCreateSerializer(data=data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)


class SaleSerializerTest(TestCase):
    def setUp(self):
        self.customer = Customer.objects.create(
            name="test", phone="84222", email="test@tes.com"
        )
        self.sales_person = SalesPerson.objects.create(
            name="test", phone="84222", email="test@tes.com"
        )
        self.product = Product.objects.create(
            code="P1", description="Product 1", value=10.0, commission_percentage=5
        )
        self.sale = Sale.objects.create(
            invoice_number="INV001",
            customer=self.customer,
            sales_person=self.sales_person,
            created_at=datetime.now(),
        )
        self.sale_item = SaleItem.objects.create(
            sale=self.sale, product=self.product, quantity=2, commission=5
        )

    def test_serializer_fields(self):
        serializer = SaleSerializer(instance=self.sale)

        expected_fields = [
            "id",
            "products_set",
            "customer_name",
            "sales_person_name",
            "invoice_number",
            "total_value",
            "created_at",
        ]

        self.assertEqual(set(serializer.fields.keys()), set(expected_fields))

    def test_get_products_set(self):
        serializer = SaleSerializer(instance=self.sale)
        products_set = serializer.get_products_set(self.sale)

        self.assertEqual(len(products_set), 1)
        self.assertEqual(products_set[0]["id"], self.product.id)
        self.assertEqual(products_set[0]["name"], self.product.description)
        self.assertEqual(products_set[0]["value"], self.product.value)
        self.assertEqual(products_set[0]["quantity"], self.sale_item.quantity)
        self.assertEqual(
            products_set[0]["commission_percentage"],
            self.product.commission_percentage,
        )

    def test_get_total_value(self):
        serializer = SaleSerializer(instance=self.sale)
        total_value = serializer.get_total_value(self.sale)

        expected_total_value = self.sale_item.total_value
        self.assertEqual(total_value, expected_total_value)

    def test_required_fields(self):
        serializer = SaleSerializer(data={})

        self.assertFalse(serializer.is_valid())
        self.assertIn("invoice_number", serializer.errors)

    def test_datetime_validation(self):
        data = {
            "invoice_number": "INV002",
            "customer": 1,
            "sales_person": 1,
            "created_at": "2023-09-25T10:00:00Z",
        }
        serializer = SaleSerializer(data=data)

        self.assertTrue(serializer.is_valid())

        data["created_at"] = "25-09-2023 10:00:00"
        serializer = SaleSerializer(data=data)

        self.assertFalse(serializer.is_valid())

    def test_field_types(self):
        data = {
            "invoice_number": "INV003",
            "customer": 1,
            "sales_person": 1,
            "created_at": "2023-09-25T10:00:00Z",
        }
        serializer = SaleSerializer(data=data)

        self.assertTrue(serializer.is_valid())

        self.assertIsInstance(serializer.validated_data["invoice_number"], str)
