from uuid import uuid4
from datetime import datetime
from rest_framework import viewsets
from sales.models import Sale, SaleItem
from sales.serializers import SaleSerializer, SaleItemSerializer, SalesCreateSerializer
from rest_framework.response import Response
from rest_framework import status
from django.db import transaction
from commissions.services import CommissionService
from product.models import Product
from django.utils.timezone import make_aware


class SaleItemViewSet(viewsets.ModelViewSet):
    queryset = SaleItem.objects.all()
    serializer_class = SaleItemSerializer


class SaleViewSet(viewsets.ModelViewSet):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        products = data.pop("products")
        serializer = SalesCreateSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        if not products:
            return Response(
                {"message": "Não existem produtos nessa venda."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        with transaction.atomic():
            created_at = (
                datetime.strptime(
                    serializer.data["created_at"], "%Y-%m-%dT%H:%M:%S.%fZ"
                )
                if serializer.data["created_at"]
                else datetime.now()
            )
            sale = self.queryset.create(
                customer_id=serializer.data["customer"],
                sales_person_id=serializer.data["sales_person"],
                created_at=created_at,
                invoice_number=str(uuid4()),
            )
            commission_day = CommissionService().get_percentage_commission(
                sale.created_at
            )
            for product in products:
                prod = Product.objects.get(id=product["product"]["id"])
                actually_commission = prod.commission_percentage
                if commission_day is not None:
                    actually_commission = CommissionService().determine_commission(
                        commission_day.min_commission,
                        commission_day.max_commission,
                        actually_commission,
                    )
                SaleItem.objects.create(
                    sale=sale,
                    product_id=product["product"]["id"],
                    quantity=product["quantity"],
                    commission=actually_commission,
                )

            serializer = self.serializer_class(sale)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
