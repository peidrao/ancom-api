# Generated by Django 4.2.5 on 2023-10-08 00:47

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("product", "0001_initial"),
        ("persons", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Sale",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "invoice_number",
                    models.CharField(
                        max_length=50, verbose_name="Número da nota fiscal"
                    ),
                ),
                ("created_at", models.DateTimeField(verbose_name="Data/Hora")),
                (
                    "customer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="persons.customer",
                        verbose_name="Cliente",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="SaleItem",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "commission",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=4,
                        validators=[
                            django.core.validators.MinValueValidator(0),
                            django.core.validators.MaxValueValidator(10),
                        ],
                        verbose_name="Percentual de Comissão",
                    ),
                ),
                (
                    "quantity",
                    models.PositiveIntegerField(verbose_name="Quantidades Vendidas"),
                ),
                (
                    "product",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="product.product",
                    ),
                ),
                (
                    "sale",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT, to="sales.sale"
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="sale",
            name="products",
            field=models.ManyToManyField(
                through="sales.SaleItem", to="product.product", verbose_name="Produtos"
            ),
        ),
        migrations.AddField(
            model_name="sale",
            name="sales_person",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                to="persons.salesperson",
                verbose_name="Vendedor",
            ),
        ),
    ]
