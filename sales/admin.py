from django.contrib import admin
from sales.models import Sale, SaleItem


class SaleItemInline(admin.TabularInline):
    model = SaleItem
    extra = 1


class SaleAdmin(admin.ModelAdmin):
    list_display = ("invoice_number", "customer", "sales_person", "created_at")
    inlines = [SaleItemInline]


class SaleItemAdmin(admin.ModelAdmin):
    list_display = ("sale", "product", "quantity", "total_value")


admin.site.register(Sale, SaleAdmin)
admin.site.register(SaleItem, SaleItemAdmin)
