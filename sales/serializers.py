from rest_framework import serializers
from sales.models import Sale, SaleItem


class SaleItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = SaleItem
        fields = "__all__"


class SaleSerializer(serializers.ModelSerializer):
    products_set = serializers.SerializerMethodField()
    sales_person_name = serializers.CharField(
        source="sales_person.name", read_only=True
    )
    customer_name = serializers.CharField(source="customer.name", read_only=True)
    total_value = serializers.SerializerMethodField()

    class Meta:
        model = Sale
        fields = [
            "id",
            "products_set",
            "customer_name",
            "sales_person_name",
            "invoice_number",
            "total_value",
            "created_at",
        ]

    def get_products_set(self, obj):
        products_list = []

        for item in obj.saleitem_set.all():
            product_dict = {}
            product_dict["id"] = item.product.id
            product_dict["name"] = item.product.description
            product_dict["value"] = item.product.value
            product_dict["quantity"] = item.quantity
            product_dict["commission_percentage"] = item.commission
            product_dict["commission"] = item.commission_value
            product_dict["total_value"] = item.total_value
            products_list.append(product_dict)

        return products_list

    def get_total_value(self, obj):
        total_amount = 0

        for item in obj.saleitem_set.all():
            total_amount += item.total_value

        return total_amount


class SalesCreateSerializer(serializers.Serializer):
    customer = serializers.IntegerField()
    sales_person = serializers.IntegerField()
    created_at = serializers.DateTimeField()
