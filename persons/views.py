from rest_framework import viewsets
from persons.models import Customer, SalesPerson
from persons.serializers import CustomerSerializer, SalesPersonSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class SalesPersonViewSet(viewsets.ModelViewSet):
    queryset = SalesPerson.objects.all()
    serializer_class = SalesPersonSerializer
