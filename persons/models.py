from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nome")
    email = models.EmailField(max_length=100, verbose_name="E-mail")
    phone = models.CharField(max_length=40, verbose_name="Número de telefone")

    class Meta:
        abstract = True


class Customer(Person):
    def __str__(self):
        return f"Cliente: {self.name}"


class SalesPerson(Person):
    def __str__(self):
        return f"Vendedor: {self.name}"
