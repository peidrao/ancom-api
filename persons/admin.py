from django.contrib import admin

from persons.models import Customer, SalesPerson


class CustomerAdmin(admin.ModelAdmin):
    list_display = ("name", "email", "phone")
    search_fields = ("name", "email", "phone")
    list_filter = ()


class SalesPersonAdmin(admin.ModelAdmin):
    list_display = ("name", "email", "phone")
    search_fields = ("name", "email", "phone")
    list_filter = ()


# admin.site.register(Person, PersonAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(SalesPerson, SalesPersonAdmin)
