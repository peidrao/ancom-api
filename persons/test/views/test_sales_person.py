from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from persons.models import SalesPerson


class SalesPersonViewSetTests(APITestCase):
    def setUp(self):
        self.sales_person1 = SalesPerson.objects.create(
            name="SalesPerson 1", email="sales1@example.com", phone="1234567890"
        )
        self.sales_person2 = SalesPerson.objects.create(
            name="SalesPerson 2", email="sales2@example.com", phone="9876543210"
        )

    def test_get_all_sales_people(self):
        url = reverse("salesperson-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get_single_sales_person(self):
        url = reverse("salesperson-detail", kwargs={"pk": self.sales_person1.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], self.sales_person1.name)

    def test_create_sales_person(self):
        url = reverse("salesperson-list")
        data = {
            "name": "New SalesPerson",
            "email": "newsalesperson@example.com",
            "phone": "9999999999",
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(SalesPerson.objects.count(), 3)

    def test_update_sales_person(self):
        url = reverse("salesperson-detail", kwargs={"pk": self.sales_person1.pk})
        data = {"name": "Updated SalesPerson"}
        response = self.client.patch(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            SalesPerson.objects.get(pk=self.sales_person1.pk).name,
            "Updated SalesPerson",
        )

    def test_delete_sales_person(self):
        url = reverse("salesperson-detail", kwargs={"pk": self.sales_person1.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(SalesPerson.objects.count(), 1)

    def test_create_sales_person_invalid_data(self):
        url = reverse("salesperson-list")
        data = {"name": "New SalesPerson"}
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(SalesPerson.objects.count(), 2)
