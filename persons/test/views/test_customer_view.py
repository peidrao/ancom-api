from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from persons.models import Customer


class CustomerViewSetTests(APITestCase):
    def setUp(self):
        self.customer1 = Customer.objects.create(
            name="Customer 1", email="customer1@example.com", phone="1234567890"
        )
        self.customer2 = Customer.objects.create(
            name="Customer 2", email="customer2@example.com", phone="9876543210"
        )

    def test_get_all_customers(self):
        url = reverse("customer-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_get_single_customer(self):
        url = reverse("customer-detail", kwargs={"pk": self.customer1.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], self.customer1.name)

    def test_create_customer(self):
        url = reverse("customer-list")
        data = {
            "name": "New Customer",
            "email": "newcustomer@example.com",
            "phone": "9999999999",
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Customer.objects.count(), 3)

    def test_update_customer(self):
        url = reverse("customer-detail", kwargs={"pk": self.customer1.pk})
        data = {"name": "Updated Customer"}
        response = self.client.patch(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            Customer.objects.get(pk=self.customer1.pk).name, "Updated Customer"
        )

    def test_delete_customer(self):
        url = reverse("customer-detail", kwargs={"pk": self.customer1.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Customer.objects.count(), 1)
