from django.test import TestCase
from persons.models import Customer


class CustomerModelTests(TestCase):
    def setUp(self):
        self.customer = Customer.objects.create(
            name="John Doe", email="john@example.com", phone="1234567890"
        )

    def test_customer_creation(self):
        self.assertEqual(self.customer.name, "John Doe")
        self.assertEqual(self.customer.email, "john@example.com")
        self.assertEqual(self.customer.phone, "1234567890")

    def test_customer_str_method(self):
        self.assertEqual(str(self.customer), "Cliente: John Doe")

    def test_customer_name_max_length(self):
        max_length = self.customer._meta.get_field("name").max_length
        self.assertEqual(max_length, 100)

    def test_customer_email_max_length(self):
        max_length = self.customer._meta.get_field("email").max_length
        self.assertEqual(max_length, 100)

    def test_customer_phone_max_length(self):
        max_length = self.customer._meta.get_field("phone").max_length
        self.assertEqual(max_length, 40)
