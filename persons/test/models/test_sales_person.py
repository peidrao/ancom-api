from django.test import TestCase
from persons.models import SalesPerson


class SalesPersonModelTests(TestCase):
    def setUp(self):
        self.sales_person = SalesPerson.objects.create(
            name="Alice Smith", email="alice@example.com", phone="1234567890"
        )

    def test_sales_person_creation(self):
        self.assertEqual(self.sales_person.name, "Alice Smith")
        self.assertEqual(self.sales_person.email, "alice@example.com")
        self.assertEqual(self.sales_person.phone, "1234567890")

    def test_sales_person_str_method(self):
        self.assertEqual(str(self.sales_person), "Vendedor: Alice Smith")

    def test_sales_person_name_max_length(self):
        max_length = self.sales_person._meta.get_field("name").max_length
        self.assertEqual(max_length, 100)

    def test_sales_person_email_max_length(self):
        max_length = self.sales_person._meta.get_field("email").max_length
        self.assertEqual(max_length, 100)

    def test_sales_person_phone_max_length(self):
        max_length = self.sales_person._meta.get_field("phone").max_length
        self.assertEqual(max_length, 40)
