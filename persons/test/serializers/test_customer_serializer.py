from django.test import TestCase
from persons.models import Customer
from persons.serializers import CustomerSerializer


class CustomerSerializerTest(TestCase):
    def setUp(self):
        self.customer_data = {
            "name": "John Doe",
            "email": "john@example.com",
            "phone": "1234567890",
        }

    def test_valid_data(self):
        serializer = CustomerSerializer(data=self.customer_data)
        self.assertTrue(serializer.is_valid())

    def test_missing_required_field(self):
        # Test when a required field is missing
        del self.customer_data["name"]
        serializer = CustomerSerializer(data=self.customer_data)
        self.assertFalse(serializer.is_valid())
