from django.test import TestCase
from persons.models import SalesPerson
from persons.serializers import SalesPersonSerializer


class SalesPersonSerializerTest(TestCase):
    def setUp(self):
        self.salesperson_data = {
            "name": "Alice Smith",
            "email": "alice@example.com",
            "phone": "9876543210",
        }

    def test_valid_data(self):
        serializer = SalesPersonSerializer(data=self.salesperson_data)
        self.assertTrue(serializer.is_valid())

    def test_missing_required_field(self):
        # Test when a required field is missing
        del self.salesperson_data["email"]
        serializer = SalesPersonSerializer(data=self.salesperson_data)
        self.assertFalse(serializer.is_valid())
