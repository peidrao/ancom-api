from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CustomerViewSet, SalesPersonViewSet

router = DefaultRouter()
router.register(r"customers", CustomerViewSet)
router.register(r"salespersons", SalesPersonViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
