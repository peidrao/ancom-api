# Amcom-API

A API do projeto foi desenvolvida com todas as tarefas obrigatórias solicitadas no desafio.

O maior desafio foi compreender a regra de negócio relacionada à criação das vendas e suas respectivas comissões.

## Pontos que faria diferente.

Trabalharia melhor nos services para deixá-los encarregados das regras de negócio da aplicação. Não são muitas regras, mas isso traria uma organização melhor à aplicação e a escalaria de forma mais eficaz.

## Como Rodar o Projeto

Aqui estão as instruções para rodar o projeto localmente.

### Pré-requisitos

- Python 3.x
- pip (gerenciador de pacotes do Python)
- Docker e Docker-compose

### Configuração do Ambiente Virtual (recomendado)

1. Crie um ambiente virtual para o projeto:

```bash
python3 -m venv myenv
```

2. Ative o ambiente virtual (Unix ou MacOS):

```bash
source myenv/bin/activate
```

### Instalação das Dependências

1. Instale as dependências do projeto:

```bash
pip install -r requirements.txt
```

### Criar container docker (para uso de banco de dados)

```bash
docker-compose up -d
```

### Configuração do Banco de Dados

1. Aplique as migrações:

```bash
python manage.py migrate
```

2. Crie um superusuário (para acessar o Django Admin):

```bash
python manage.py createsuperuser
```

### Rodando o Servidor

Inicie o servidor de desenvolvimento:

```bash
python manage.py runserver
```

Acesse a aplicação no navegador: http://localhost:8000

### Populando aplicação

Para evitar ter que criar objetos no bancos de dados podemos usar dois comandos um para configuração de dias e outro que popula os dados das seguintes tabelas (Customer, SalesPerson, Sale, SaleItem, Product)

## Popular dias da semana

```bash
python manage.py populate_commissions
```
## Popular as outras tabelas

```bash
python manage.py populate_sales
```

## Endpoints do Projeto

- `/swagger/`: Documentação dos endpoints usados
- `/commissions/` : Filtro com os vendedores e suas respectivas comissões [GET].
- `/customers/`: CRUD para clientes.
- `/salespersons/`: CRUD para vendedores.
- `/products/`: CRUD para produtos.
- `/sales/`: CRUD para vendas.

## Bibliotecas Usadas

- Django.
- Django Rest Framework
- drf-yasg: Swagger/OpenAPI
- Faker: gerador de dados fictícios
- black: formatador de código

## Testes unitários

- A aplicação conta com **69 testes unitários**. 
- Possui 99% de cobertura de testes

## Atalhos

Foi criado um arquivo Makefile para resumir alguns dos comandos que normalmente utilizamos no desenvolvimento.

Ex. `make run` -> Rodar o projeto

| Atalho   | Comando                          | Função                                   |
| -------- | -------------------------------- | ---------------------------------------- |
| run      | manage.py runserver              | Subir projeto                            |
| make     | manage.py makemigrations         | Criar arquivo de migração                |
| migrate  | manage.py migrate                | Criar migração no banco de dados         |
| test     | manage.py test                   | Rodar os testes                          |
| shell    | coverage run manage.py test      | Rodar shell do django                    |
| coverage | coverage run manage.py test      | Rodar testes para relatório de cobertura |
| html     | coverage html                    | Criar relatório de cobertura             |
| fmt      | black .                          | Formatar código                          |
| db.up    | docker-compose up -d             | Subir/Criar container docker             |
| db.down  | docker stop gym-series-python-db | Parar container docker                    |

