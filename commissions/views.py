from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from commissions.serializers import (
    CommissionsReportSerializer,
    SalespersonCommissionSerializer,
)
from persons.models import SalesPerson
from sales.models import Sale


class CommissionsReport(APIView):
    serializer_class = SalespersonCommissionSerializer

    def get(self, request):
        serializer = CommissionsReportSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        sales_in_period = Sale.objects.filter(
            created_at__gte=serializer.validated_data["initial_period"],
            created_at__lte=serializer.validated_data["final_period"],
        )
        salesperson = SalesPerson.objects.filter(
            id__in=sales_in_period.values_list("sales_person").distinct()
        )

        serializer = SalespersonCommissionSerializer(
            salesperson, many=True, context={"sales_in_period": sales_in_period}
        )

        return Response(serializer.data, status=status.HTTP_200_OK)
