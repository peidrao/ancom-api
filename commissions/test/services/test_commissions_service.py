from django.test import TestCase
from commissions.models import CommissionConfig
from commissions.services import CommissionService
from django.utils import timezone


class CommissionServiceTests(TestCase):
    def setUp(self):
        self.config_monday = CommissionConfig.objects.create(
            day_of_week="Monday", min_commission=3, max_commission=5, is_active=True
        )
        self.config_tuesday = CommissionConfig.objects.create(
            day_of_week="Tuesday", min_commission=2, max_commission=6, is_active=True
        )
        self.config_inactive = CommissionConfig.objects.create(
            day_of_week="Saturday", min_commission=1, max_commission=7, is_active=False
        )

        self.service = CommissionService()

    def test_get_percentage_commission_inactive(self):
        saturday = timezone.now().replace(
            hour=0, minute=0, second=0, microsecond=0
        ) + timezone.timedelta(days=5)

        self.assertIsNone(self.service.get_percentage_commission(saturday))

    def test_determine_commission(self):
        min_commission = 3
        max_commission = 5

        commission_below_min = 2
        self.assertEqual(
            self.service.determine_commission(
                min_commission, max_commission, commission_below_min
            ),
            min_commission,
        )

        commission_in_range = 5
        self.assertEqual(
            self.service.determine_commission(
                min_commission, max_commission, commission_in_range
            ),
            commission_in_range,
        )

        commission_above_max = 6
        self.assertEqual(
            self.service.determine_commission(
                min_commission, max_commission, commission_above_max
            ),
            max_commission,
        )
