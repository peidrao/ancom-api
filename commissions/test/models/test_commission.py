from django.test import TestCase
from commissions.models import CommissionConfig


class CommissionConfigTests(TestCase):
    def setUp(self):
        CommissionConfig.objects.create(
            day_of_week="monday", min_commission=3, max_commission=5
        )
        CommissionConfig.objects.create(
            day_of_week="tuesday", min_commission=2, max_commission=6
        )

    def test_commission_str(self):
        monday_config = CommissionConfig.objects.get(day_of_week="monday")

        self.assertEqual(monday_config.__str__(), "Commission Configuration for Monday")

    def test_commission_config_exists(self):
        monday_config = CommissionConfig.objects.get(day_of_week="monday")
        tuesday_config = CommissionConfig.objects.get(day_of_week="tuesday")

        self.assertEqual(monday_config.min_commission, 3)
        self.assertEqual(monday_config.max_commission, 5)
        self.assertEqual(tuesday_config.min_commission, 2)
        self.assertEqual(tuesday_config.max_commission, 6)
