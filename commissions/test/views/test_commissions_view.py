import warnings
from datetime import datetime
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from sales.models import Sale
from persons.models import Customer, SalesPerson
from product.models import Product
from commissions.views import CommissionsReport


warnings.filterwarnings("ignore", message="DateTimeField .* received a naive datetime")


class CommissionsReportTests(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.customer = Customer.objects.create(name="Customer Test")
        self.sales_person = SalesPerson.objects.create(name="Sales Person Test")
        self.product = Product.objects.create(
            code="PROD1", description="Product 1", value=10.0, commission_percentage=5.0
        )
        self.sale = Sale.objects.create(
            invoice_number="INV123",
            customer=self.customer,
            sales_person=self.sales_person,
            created_at=datetime.now(),
        )

    def test_commissions_report_view(self):
        url = reverse("commissions")
        query_params = {"initial_period": "2023-01-01", "final_period": "2023-12-31"}
        request = self.factory.get(url, query_params)

        view = CommissionsReport.as_view()
        response = view(request)

        self.assertEqual(response.status_code, 200)
