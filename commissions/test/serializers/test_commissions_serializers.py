from decimal import Decimal
from datetime import datetime
from django.test import TestCase
from rest_framework.exceptions import ValidationError
from datetime import date
from commissions.serializers import (
    CommissionsReportSerializer,
    SalespersonCommissionSerializer,
)
from persons.models import SalesPerson, Customer
from sales.models import Sale, SaleItem
from product.models import Product


class CommissionsReportSerializerTests(TestCase):
    def test_valid_dates(self):
        data = {"initial_period": date(2023, 1, 1), "final_period": date(2023, 12, 31)}
        serializer = CommissionsReportSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_initial_period_greater_than_final(self):
        data = {"initial_period": date(2023, 12, 31), "final_period": date(2023, 1, 1)}
        serializer = CommissionsReportSerializer(data=data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)


class SalespersonCommissionSerializerTests(TestCase):
    def setUp(self):
        self.sales_person = SalesPerson.objects.create(name="John Doe")
        self.customer = Customer.objects.create(name="John Doe")
        self.sale1 = Sale.objects.create(
            invoice_number="INV-001",
            sales_person=self.sales_person,
            customer=self.customer,
            created_at=datetime.now(),
        )
        self.sale2 = Sale.objects.create(
            invoice_number="INV-002",
            sales_person=self.sales_person,
            customer=self.customer,
            created_at=datetime.now(),
        )

    def test_serializer_with_no_sales(self):
        serializer = SalespersonCommissionSerializer(instance=self.sales_person)
        expected_data = {
            "id": 1,
            "name": "John Doe",
            "commission": {"total": 0, "count": 0},
        }
        self.assertEqual(serializer.data, expected_data)

    def test_serializer_with_sales_no_items(self):
        Sale.objects.create(
            invoice_number="INV-003",
            sales_person=self.sales_person,
            customer=self.customer,
            created_at=datetime.now(),
        )
        serializer = SalespersonCommissionSerializer(instance=self.sales_person)
        expected_data = {
            "id": 2,
            "name": "John Doe",
            "commission": {"total": 0, "count": 0},
        }
        self.assertEqual(serializer.data, expected_data)
