from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _


class DayChoices(models.TextChoices):
    MONDAY = "monday", _("Monday")
    TUESDAY = "tuesday", _("Tuesday")
    WEDNESDAY = "wednesday", _("Wednesday")
    THURSDAY = "thursday", _("Thursday")
    FRIDAY = "friday", _("Friday")
    SATURDAY = "saturday", _("Saturday")
    SUNDAY = "sunday", _("Sunday")


class CommissionConfig(models.Model):
    day_of_week = models.CharField(
        _("Day of the Week"), max_length=10, choices=DayChoices.choices, unique=True
    )
    min_commission = models.FloatField(
        _("Minimum Commission"),
        validators=[MinValueValidator(0), MaxValueValidator(10)],
    )
    max_commission = models.FloatField(
        _("Maximum Commission"),
        validators=[MinValueValidator(0), MaxValueValidator(10)],
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f"Commission Configuration for {self.get_day_of_week_display()}"

    class Meta:
        verbose_name = _("Commission Configuration")
        verbose_name_plural = _("Commission Configurations")
