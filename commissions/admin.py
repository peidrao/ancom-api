from django.contrib import admin
from commissions.models import CommissionConfig


class CommissionConfigAdmin(admin.ModelAdmin):
    list_display = ("day_of_week", "min_commission", "max_commission", "is_active")
    list_editable = ("min_commission", "max_commission", "is_active")
    readonly_fields = ("day_of_week",)

    def has_add_permission(self, request):
        return False


admin.site.register(CommissionConfig, CommissionConfigAdmin)
