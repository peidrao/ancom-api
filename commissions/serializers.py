from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from sales.models import Sale


class CommissionsReportSerializer(serializers.Serializer):
    final_period = serializers.DateField()
    initial_period = serializers.DateField()

    def validate(self, data):
        initial_period = data.get("initial_period")
        final_period = data.get("final_period")

        if initial_period and final_period and initial_period > final_period:
            raise serializers.ValidationError(
                _("initial_period cannot be greater than final_period.")
            )

        return data


class SalespersonCommissionSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    commission = serializers.SerializerMethodField()

    def get_commission(self, obj):
        total_commission = 0
        total_sales = 0
        sales = self.context.get("sales_in_period")

        if sales:
            for sale in sales.filter(sales_person=obj):
                total_sales += 1
                for item in sale.saleitem_set.all():
                    total_commission += item.commission_value

        return {"total": total_commission, "count": total_sales}
