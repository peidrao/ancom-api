from commissions.models import CommissionConfig


class CommissionService:
    days = CommissionConfig.objects.filter(is_active=True)

    def get_percentage_commission(self, date) -> CommissionConfig:
        try:
            commission = self.days.get(day_of_week=date.strftime("%A").lower())
        except CommissionConfig.DoesNotExist:
            commission = None

        return commission

    def determine_commission(self, min_commission, max_commission, actually_commission):
        if actually_commission < min_commission:
            return min_commission
        else:
            return max_commission
