from django.urls import path
from commissions.views import CommissionsReport

urlpatterns = [path("commissions/", CommissionsReport.as_view(), name="commissions")]
