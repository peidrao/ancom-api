DJANGO=python manage.py

run:
	$(DJANGO) runserver

make:
	$(DJANGO) makemigrations

migrate:
	$(DJANGO) migrate

test:
	$(DJANGO) test

shell:
	$(DJANGO) shell

coverage: 
	coverage run manage.py test 

html:
	coverage html

fmt: 
	black .

db.up: 
	docker-compose up -d

db.down:
	docker stop gym-series-python-db
